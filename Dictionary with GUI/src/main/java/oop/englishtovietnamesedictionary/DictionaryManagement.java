package oop.englishtovietnamesedictionary;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class DictionaryManagement {
    public static void insertFromFile() {
        try {
            Scanner myReader = new Scanner(new File("dictionaries.txt"));
            while (myReader.hasNextLine()) {
                String key = myReader.nextLine();
                ArrayList<String> values = new ArrayList<>();
                while (!myReader.hasNextBoolean()) {
                    values.add(myReader.nextLine());
                }
                myReader.nextLine();
                Dictionary.getWordList().put(key, values);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void dictionaryExportToFile() {
        try {
            File data = new File("dictionaries.txt");
            if (data.exists()) {
                data.delete();
            }
            data.createNewFile();

            BufferedWriter bw = new BufferedWriter(new FileWriter("dictionaries.txt"));
            for (Map.Entry<String, ArrayList<String>> entry : Dictionary.getWordList().entrySet()) {
                bw.append(entry.getKey() + "\n");
                for (int i = 0, n = entry.getValue().size(); i < n; i++) {
                    bw.append(entry.getValue().get(i) + "\n");
                }
                bw.append("true" + "\n");
            }
            bw.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}

package oop.englishtovietnamesedictionary;

import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


public class AutocompleteTextField extends TextField {
    // All applicable entries.
    private final SortedSet<String> entries;
    // The autocomplete dropdown.
    private ContextMenu suggestionDropdown;

    public SortedSet<String> getEntries() {
        return entries;
    }

    public AutocompleteTextField() {
        super();
        this.entries = new TreeSet<>();
        this.suggestionDropdown = new ContextMenu();

        // Modify suggestions as entered text changes.
        textProperty().addListener((observable, oldValue, newValue) -> {
            String inputText = getText();
            // Hide suggestions if textField is empty.
            if (inputText == null || inputText.isEmpty()) {
                suggestionDropdown.hide();
            } else {
                // Put all found suggestions in a list (case-insensitive).
                List<String> foundEntries = new ArrayList<>();
                for (String entry : entries) {
                    if (entry.toLowerCase().contains(inputText.toLowerCase())) {
                        foundEntries.add(entry);
                    }
                }
                if (!foundEntries.isEmpty()) {
                    // Populate the dropdown menu.
                    populatePopup(foundEntries, inputText);
                    if (!suggestionDropdown.isShowing()) {
                        suggestionDropdown.show(AutocompleteTextField.this, Side.BOTTOM, 0, 0);
                    }
                    // Hide the dropdown menu should there be no suggestions.
                } else {
                    suggestionDropdown.hide();
                }
            }
        });

        // Hide the dropdown menu should the textField lose (or gain) focus.
        focusedProperty().addListener((observableValue, oldValue, newValue) -> {
            suggestionDropdown.hide();
        });
    }

    /**
     * Build TextFlow inside dropdown menu.
     */
    public static TextFlow buildTextFlow(String text, String filter) {
        int filterIndex = text.toLowerCase().indexOf(filter.toLowerCase());
        Text textBefore = new Text(text.substring(0, filterIndex));
        Text textAfter = new Text(text.substring(filterIndex + filter.length()));
        Text textFilter = new Text(text.substring(filterIndex,  filterIndex + filter.length()));
        textFilter.setFill(Color.ROYALBLUE);
        textFilter.setFont(Font.font("Helvetica", FontWeight.BOLD, 12));
        return new TextFlow(textBefore, textFilter, textAfter);
    }

    /**
     * Populate the dropdown menu with the search results based on input. Display limited to 10 entries.
     *
     * @param searchResult The list of matching strings.
     */
    private void populatePopup(List<String> searchResult, String searchInput) {
        // The list that will contain suggestions.
        List<CustomMenuItem> menuItems = new ArrayList<>();

        final int maxSuggestions = 10;
        int count = Math.min(searchResult.size(), maxSuggestions);
        // Build the list with labels.
        for (int i = 0; i < count; i++) {
            final String result = searchResult.get(i);
            // Label utilizing textFlow to display suggestions.
            Label entryLabel = new Label();
            entryLabel.setGraphic(buildTextFlow(result, searchInput));
            entryLabel.setPrefHeight(10);
            CustomMenuItem item = new CustomMenuItem(entryLabel, true);
            menuItems.add(item);

            // Close the dropdown menu upon selecting any suggestion.
            item.setOnAction(e -> {
                setText(result);
                positionCaret(result.length());
                suggestionDropdown.hide();
            });
        }

        // Refresh the dropdown menu before adding new items.
        suggestionDropdown.getItems().clear();
        suggestionDropdown.getItems().addAll(menuItems);
    }
}
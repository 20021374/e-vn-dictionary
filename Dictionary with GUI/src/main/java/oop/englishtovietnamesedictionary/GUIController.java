package oop.englishtovietnamesedictionary;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaException;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class GUIController implements Initializable {
    @FXML
    private AutocompleteTextField searchBar;

    @FXML
    private TextFlow resultArea;

    @FXML
    private TextFlow indexList;

    @FXML
    private Button searchButton;

    @FXML
    private Button editButton;

    @FXML
    private Button removeButton;

    @FXML
    private Button addButton;

    @FXML
    private Button audioButton;

    @FXML
    private Button helpButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        DictionaryManagement.insertFromFile();

        ArrayList<String> listOfWords = new ArrayList<>(Dictionary.getWordList().keySet());
        for (String word : listOfWords) {
            addEntryAsHyperLink(word);
        }

        searchBar.getEntries().addAll(listOfWords);
    }

    @FXML
    private void searchEntry(ActionEvent actionEvent) throws FileNotFoundException {
        if (searchBar.getText().matches(".*\\d+.*") || searchBar.getText().length() == 0) {
            resultArea.getChildren().clear();
            Text warning = new Text("Input not recognized or accepted! Please try again.");
            warning.setFill(Color.RED);
            resultArea.getChildren().add(warning);
        } else if (searchBar.getText().contains(" ")) {
            resultArea.getChildren().clear();
            Text warning = new Text("Input should not contain spaces! Please try again.");
            warning.setFill(Color.RED);
            resultArea.getChildren().add(warning);
        } else {
            String searchEntry = searchBar.getText().toLowerCase();
            if (!Dictionary.getWordList().containsKey(searchEntry)) {
                resultArea.getChildren().clear();
                Text message = new Text("Entry does not (yet) exist in database.");
                message.setFill(Color.ORANGE);
                resultArea.getChildren().add(message);
                audioButton.setVisible(false);
                removeButton.setVisible(false);
                editButton.setVisible(false);
            } else {
                displayEntry(searchEntry);
            }
        }
    }

    @FXML
    public void exitApplication(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void editEntry(ActionEvent actionEvent) {
        Stage popupWindow = new Stage();
        popupWindow.initModality(Modality.APPLICATION_MODAL);
        popupWindow.setTitle("Entry Editor");

        Label entryLabel = new Label("Entry");
        entryLabel.setLayoutX(5);
        entryLabel.setLayoutY(53);

        Label pronunciationLabel = new Label("Pronunciation");
        pronunciationLabel.setLayoutX(5);
        pronunciationLabel.setLayoutY(78);

        Label meaningLabel = new Label("Meaning");
        meaningLabel.setLayoutX(5);
        meaningLabel.setLayoutY(103);

        Label etymologyLabel = new Label("Etymology");
        etymologyLabel.setLayoutX(5);
        etymologyLabel.setLayoutY(303);

        Label audioLabel = new Label("Link to audio");
        audioLabel.setLayoutX(5);
        audioLabel.setLayoutY(328);

        TextField entryText = new TextField();
        entryText.setPrefWidth(100);
        entryText.setLayoutX(100);
        entryText.setLayoutY(50);

        TextField pronunciationText = new TextField();
        pronunciationText.setPrefWidth(100);
        pronunciationText.setLayoutX(100);
        pronunciationText.setLayoutY(75);

        TextArea meaningText = new TextArea();
        meaningText.setLayoutX(100);
        meaningText.setLayoutY(100);

        TextField etymologyText = new TextField();
        etymologyText.setPrefWidth(500);
        etymologyText.setLayoutX(100);
        etymologyText.setLayoutY(300);

        TextField audioLink = new TextField();
        audioLink.setPrefWidth(500);
        audioLink.setLayoutX(100);
        audioLink.setLayoutY(325);

        Button buttonCancel = new Button("Cancel");
        Button buttonConfirm = new Button("OK");

        ArrayList<String> entryProperties = new ArrayList<>();
        for (Node node : resultArea.getChildren()) {
            if (node instanceof Text) {
                if (!((Text) node).getText().equals("\n")) {
                    entryProperties.add(((Text) node).getText());
                }
            }
        }
        entryText.setText(entryProperties.get(0));
        pronunciationText.setText(entryProperties.get(1));
        int i = 2;
        while (!entryProperties.get(i).equals("Etymology")) {
            meaningText.appendText(entryProperties.get(i) + "\n");
            i++;
        }
        etymologyText.setText(entryProperties.get(i+1));
        audioLink.setText(Dictionary.getWordList().get(entryProperties.get(0))
                .get(Dictionary.getWordList().get(entryProperties.get(0)).size()-1));

        buttonCancel.setOnAction(e -> popupWindow.close());
        buttonConfirm.setOnAction(e -> {
            entryProperties.clear();
            entryProperties.add(pronunciationText.getText());
            ArrayList<String> meaningLines = new ArrayList<>
                    (Arrays.asList(meaningText.getText().split("\n")));
            entryProperties.addAll(meaningLines);
            entryProperties.add("Etymology");
            entryProperties.add(etymologyText.getText());
            entryProperties.add(audioLink.getText());

            String oldEntry = ((Text) resultArea.getChildren().get(0)).getText();
            Dictionary.getWordList().remove(oldEntry);
            searchBar.getEntries().remove(oldEntry);
            for (Node node : indexList.getChildren()) {
                if (node instanceof Hyperlink) {
                    if (((Hyperlink) node).getText().equals(oldEntry)) {
                        indexList.getChildren().remove(node);
                        break;
                    }
                }
            }

            String editedEntry = entryText.getText().toLowerCase();
            Dictionary.getWordList().put(editedEntry, entryProperties);
            searchBar.getEntries().add(editedEntry);
            popupWindow.close();
            addEntryAsHyperLink(editedEntry);
            displayEntry(editedEntry);
        });

        Pane pane = new Pane();
        HBox buttonArea = new HBox(30);
        buttonArea.getChildren().addAll(buttonConfirm, buttonCancel);
        pane.getChildren().addAll(entryText, pronunciationText, meaningText, etymologyText,
                audioLabel, entryLabel, pronunciationLabel, meaningLabel, etymologyLabel,
                audioLink, buttonArea);
        buttonArea.setLayoutX(230);
        buttonArea.setLayoutY(375);
        Scene scene = new Scene(pane, 600, 400);
        popupWindow.setScene(scene);
        popupWindow.show();
    }

    @FXML
    private void removeEntry(ActionEvent actionEvent) {
        Stage popupWindow = new Stage();
        popupWindow.initModality(Modality.APPLICATION_MODAL);
        popupWindow.setTitle("Entry Removal");

        Label warningMessage = new Label("This action cannot be undone.\n" +
                "Are you sure you want to proceed?");
        warningMessage.setLayoutX(50);
        warningMessage.setLayoutY(75);

        Button buttonCancel = new Button("Cancel");
        buttonCancel.setLayoutX(70);
        buttonCancel.setLayoutY(150);
        buttonCancel.setOnAction(e -> popupWindow.close());

        Button buttonConfirm = new Button("Confirm");
        buttonConfirm.setLayoutX(140);
        buttonConfirm.setLayoutY(150);
        buttonConfirm.setOnAction(e -> {
            String wordToBeRemoved = ((Text) resultArea.getChildren().get(0)).getText();
            Dictionary.getWordList().remove(wordToBeRemoved);
            searchBar.getEntries().remove(wordToBeRemoved);

            for (Node node : indexList.getChildren()) {
                if (node instanceof Hyperlink) {
                    if (((Hyperlink) node).getText().equals(wordToBeRemoved)) {
                        indexList.getChildren().remove(node);
                        break;
                    }
                }
            }

            audioButton.setVisible(false);
            removeButton.setVisible(false);
            editButton.setVisible(false);
            resultArea.getChildren().clear();
            popupWindow.close();
        });

        Pane pane = new Pane();
        pane.getChildren().addAll(warningMessage, buttonCancel, buttonConfirm);

        Scene scene = new Scene(pane, 250, 250);
        popupWindow.setScene(scene);
        popupWindow.show();
    }

    @FXML
    private void addEntry(ActionEvent actionEvent) {
        Stage popupWindow = new Stage();
        popupWindow.initModality(Modality.APPLICATION_MODAL);
        popupWindow.setTitle("Entry Addition");

        Label entryLabel = new Label("Entry");
        entryLabel.setLayoutX(5);
        entryLabel.setLayoutY(53);

        Label pronunciationLabel = new Label("Pronunciation");
        pronunciationLabel.setLayoutX(5);
        pronunciationLabel.setLayoutY(78);

        Label meaningLabel = new Label("Meaning");
        meaningLabel.setLayoutX(5);
        meaningLabel.setLayoutY(103);

        Label etymologyLabel = new Label("Etymology");
        etymologyLabel.setLayoutX(5);
        etymologyLabel.setLayoutY(303);

        Label audioLabel = new Label("Link to audio");
        audioLabel.setLayoutX(5);
        audioLabel.setLayoutY(328);

        TextField entryText = new TextField();
        entryText.setPrefWidth(100);
        entryText.setLayoutX(100);
        entryText.setLayoutY(50);

        TextField pronunciationText = new TextField();
        pronunciationText.setPrefWidth(100);
        pronunciationText.setLayoutX(100);
        pronunciationText.setLayoutY(75);

        TextArea meaningText = new TextArea();
        meaningText.setLayoutX(100);
        meaningText.setLayoutY(100);

        TextField etymologyText = new TextField();
        etymologyText.setPrefWidth(500);
        etymologyText.setLayoutX(100);
        etymologyText.setLayoutY(300);

        TextField audioLink = new TextField();
        audioLink.setPrefWidth(500);
        audioLink.setLayoutX(100);
        audioLink.setLayoutY(325);

        Button buttonCancel = new Button("Cancel");
        buttonCancel.setOnAction(e -> popupWindow.close());

        Button buttonConfirm = new Button("OK");
        buttonConfirm.setOnAction(e -> {
            ArrayList<String> entryProperties = new ArrayList<>();
            entryProperties.add(pronunciationText.getText());
            ArrayList<String> meaningLines = new ArrayList<>
                    (Arrays.asList(meaningText.getText().split("\n")));
            entryProperties.addAll(meaningLines);
            entryProperties.add("Etymology");
            entryProperties.add(etymologyText.getText());
            entryProperties.add(audioLink.getText());

            String newEntry = entryText.getText().toLowerCase();

            if (Dictionary.getWordList().containsKey(newEntry)) {
                for (Node node : indexList.getChildren()) {
                    if (node instanceof Hyperlink) {
                        if (((Hyperlink) node).getText().equals(newEntry)) {
                            indexList.getChildren().remove(node);
                            break;
                        }
                    }
                }
            }

            searchBar.getEntries().add(newEntry);
            Dictionary.getWordList().put(newEntry, entryProperties);
            addEntryAsHyperLink(newEntry);
            displayEntry(newEntry);

            popupWindow.close();
        });

        Pane pane = new Pane();

        HBox buttonArea = new HBox(30);
        buttonArea.setLayoutX(230);
        buttonArea.setLayoutY(375);
        buttonArea.getChildren().addAll(buttonConfirm, buttonCancel);

        pane.getChildren().addAll(entryText, pronunciationText, meaningText, etymologyText,
                audioLabel, entryLabel, pronunciationLabel, meaningLabel, etymologyLabel,
                audioLink, buttonArea);

        Scene scene = new Scene(pane, 600, 400);
        popupWindow.setScene(scene);
        popupWindow.show();
    }

    private void displayEntry(String entry) {
        Platform.runLater(() -> {
            resultArea.getChildren().clear();
            ArrayList<String> entryProperties = Dictionary.getWordList().get(entry);

            Text entryText = new Text(entry);
            entryText.setFont(Font.font(20));

            Text pronunciationText = new Text(entryProperties.get(0));
            pronunciationText.setFill(Color.ROYALBLUE);
            entryText.setFont(Font.font(16));

            Text newLine1 = new Text("\n");
            Text newLine2 = new Text("\n");
            Text newLine3 = new Text("\n");
            resultArea.getChildren().addAll(entryText, newLine1, pronunciationText, newLine2, newLine3);

            int meaningIndex = 1;
            while (!entryProperties.get(meaningIndex).equals("Etymology")) {
                Text newLine = new Text("\n");
                Text meaningText = new Text(entryProperties.get(meaningIndex ));
                resultArea.getChildren().addAll(meaningText, newLine);
                meaningIndex++;
            }

            Text etymologyCategory = new Text("Etymology");
            etymologyCategory.setFill(Color.BLUE);
            etymologyCategory.setFont(Font.font(18));

            Text etymologyText = new Text(entryProperties.get(meaningIndex + 1));
            Text newLine4 = new Text("\n");
            Text newLine5 = new Text("\n");
            resultArea.getChildren().addAll(newLine4, etymologyCategory, newLine5, etymologyText);

            audioButton.setVisible(true);
            removeButton.setVisible(true);
            editButton.setVisible(true);
        });
    }

    private void addEntryAsHyperLink(String wordEntry) {
        Hyperlink entry = new Hyperlink(wordEntry);
        entry.setBorder(Border.EMPTY);
        entry.setOnAction(event -> {
            displayEntry(wordEntry);
        });
        Text newLine = new Text("\n");
        indexList.getChildren().addAll(entry, newLine);
    }

    @FXML
    private void pronounceEntry(ActionEvent actionEvent) {
        ArrayList<String> properties = Dictionary.getWordList().
                get(((Text) resultArea.getChildren().get(0)).getText());
        try {
            Media sound = new Media(properties.get(properties.size() - 1));
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
        } catch (MediaException | IllegalArgumentException e) {
            Stage popupWindow = new Stage();
            popupWindow.initModality(Modality.APPLICATION_MODAL);
            popupWindow.setTitle("Error");

            Text errorMessage = new Text("There was a problem " +
                    "with the URL containing the audio file\n" +
                    "Error message: " + e.getMessage());
            errorMessage.setFill(Color.RED);

            VBox layout = new VBox();
            layout.getChildren().add(errorMessage);
            layout.setAlignment(Pos.TOP_LEFT);
            Scene scene = new Scene(layout, 325, 100);
            popupWindow.setScene(scene);
            popupWindow.show();
        }
    }

    @FXML
    private void showHelpWindow() throws FileNotFoundException {
        Stage helpWindow = new Stage();
        helpWindow.initModality(Modality.APPLICATION_MODAL);
        helpWindow.setTitle("Help");

        InputStream stream = new FileInputStream("helpWindow.png");
        Image helpImage = new Image(stream);
        ImageView helpImageView = new ImageView(helpImage);
        Pane pane = new Pane();
        pane.getChildren().add(helpImageView);
        Scene scene = new Scene(pane, 700, 500);
        helpWindow.setScene(scene);
        helpWindow.show();
    }
}

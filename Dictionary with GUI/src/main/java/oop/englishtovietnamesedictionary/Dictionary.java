package oop.englishtovietnamesedictionary;

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Map;

public class Dictionary {
    /**
     * The treemap that will store all of our dictionary data.
     * Key will store entry word.
     * Value will store the word's properties (pronunciation, meanings, etc.)
     */
    private static Map<String, ArrayList<String>> wordList = new TreeMap<>();

    public static Map<String, ArrayList<String>> getWordList() {
        return wordList;
    }
}
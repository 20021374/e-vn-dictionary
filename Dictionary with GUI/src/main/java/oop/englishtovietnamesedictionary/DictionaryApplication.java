package oop.englishtovietnamesedictionary;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class DictionaryApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(DictionaryApplication.class.getResource("GUI.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Eng-VN Dictionary");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() {
        DictionaryManagement.dictionaryExportToFile();
    }

    public static void main(String[] args) {
        launch();
    }
}
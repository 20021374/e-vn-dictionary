package oop.englishtovietnamesedictionary;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class OxfordAPI {
    protected static String dictionarySearch(String word) {
        String language = "en";
        String lowercase_word = word.toLowerCase();

        return "https://od-api.oxforddictionaries.com/api/v2/entries/" + language + "/" + lowercase_word;
    }

    /**
     * Fetch data from Oxford database.
     * @return HashMap that contains the word's properties by category.
     */
    protected static HashMap<String, String> retrieveData(String... params) {
        String app_id = "ce6ab374";
        String app_key = "822efc15ac4ef79c41bce86e0e7921c7";

        try {
            URL url = new URL(params[0]);

            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("app_id", app_id);
            urlConnection.setRequestProperty("app_key", app_key);

            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder stringbuilder = new StringBuilder();
            String line;

            //Get all lines of the JSON file.
            while ((line = reader.readLine()) != null) {
                stringbuilder.append(line + "\n");
            }

            //return stringbuilder.toString();

            JSONObject json = new JSONObject(stringbuilder.toString());
            JSONArray results = json.getJSONArray("results");
            JSONObject json1 = results.getJSONObject(0);
            JSONArray lexicalEntries = json1.getJSONArray("lexicalEntries");
            JSONObject json2 = lexicalEntries.getJSONObject(0);
            JSONArray entries = json2.getJSONArray("entries");
            JSONObject json3 = entries.getJSONObject(0);
            JSONArray etymologies = json3.getJSONArray("etymologies");
            JSONArray pronunciations = json3.getJSONArray("pronunciations");
            JSONObject json4 = pronunciations.getJSONObject(0);

            HashMap<String, String> properties = new HashMap<>();
            properties.put("pronunciation", json4.getString("phoneticSpelling"));
            properties.put("etymology", etymologies.getString(0));
            properties.put("audioFile", json4.getString("audioFile"));

            return properties;

        } catch(IOException | JSONException e) {
            HashMap<String, String> errorMessage = new HashMap<>();
            errorMessage.put("error", e.getMessage());
            return errorMessage;
        }
    }
}

module oop.englishtovietnamesedictionary {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;
    requires javafx.media;


    opens oop.englishtovietnamesedictionary to javafx.fxml;
    exports oop.englishtovietnamesedictionary;
}
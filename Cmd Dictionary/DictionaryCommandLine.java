import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DictionaryCommandLine {
    DictionaryManagement dictionaryManagement = new DictionaryManagement();             //Initialize
    Scanner input = new Scanner(System.in);

    public int Check() {
        int n = 0;
        boolean check = false;
        while (!check) {
            try {
                n = input.nextInt();
                check = true;
            } catch (Exception e) {
                System.out.print("\n");
                System.out.print("BẠN NHẬP SAI!!!\nXIN MỜI NHẬP LẠI: ");
                input.nextLine();
            }
        }
        return n;
    }

    public void dictionaryBasic() {
        dictionaryManagement.insertFromCommandline();                        //Words are inserted into list by keyboard
        showAllWords();
        dictionaryManagement.dictionaryLookup();
        dictionaryManagement.editAnyWord();
        showAllWords();
        dictionaryManagement.deleteAnyWord();
        showAllWords();
        dictionaryManagement.addWord();
        showAllWords();
        dictionaryManagement.dictionaryExportToFile();
    }

    public void dictionaryAdvanced() {
        dictionaryManagement.insertFromFile();                                   //Words are inserted into list by file
        System.out.println("0. Thoát ứng dụng");
        System.out.println("1. Xem tất cả từ vựng");
        System.out.println("2. Tra từ bằng kí tự");
        System.out.println("3. Tra nghĩa của từ");
        System.out.println("4. Sửa từ");
        System.out.println("5. Xóa từ");
        System.out.println("6. Thêm từ");
        System.out.print("\n");
        int chooseFunction = Check();

        if (chooseFunction == 0) {
            System.out.print("\n");
            System.out.println("CHƯƠNG TRÌNH KẾT THÚC.");
            System.exit(0);
        } else {
            while (chooseFunction != 0) {
                if (chooseFunction == 1) {
                    showAllWords();
                }
                if (chooseFunction == 2) {
                    dictionaryManagement.dictionarySearcher();
                }
                if (chooseFunction == 3) {
                    dictionaryManagement.dictionaryLookup();
                }
                if (chooseFunction == 4) {
                    dictionaryManagement.editAnyWord();
                    dictionaryManagement.dictionaryExportToFile();
                }
                if (chooseFunction == 5) {
                    dictionaryManagement.deleteAnyWord();
                    dictionaryManagement.dictionaryExportToFile();
                }
                if (chooseFunction == 6) {
                    dictionaryManagement.addWord();
                    dictionaryManagement.dictionaryExportToFile();
                }
                if (chooseFunction > 6 || chooseFunction < 0 ) {
                    System.out.print("\n");
                    System.out.print("BẠN NHẬP SAI!!!\nXIN MỜI NHẬP LẠI: ");
                }
                chooseFunction = Check();
            }
            System.out.print("\n");
            System.out.println("CHƯƠNG TRÌNH KẾT THÚC!");
            System.exit(0);
        }
    }

    public void showAllWords() {
        if (Dictionary.dictionary.size() == 0) System.out.println("Không có dữ liệu trong file!!!");
        else {
            System.out.println("\n---Danh sách gồm " + Dictionary.dictionary. size() + " từ---");

            for (int i = 0; i < Dictionary.dictionary.size(); i++) {
                String w_target  = (Dictionary.dictionary).get(i). getWord_target();             //Get target
                String w_explain = (Dictionary.dictionary).get(i). getWord_explain();            //Get explain
                System.out.printf("%-15s %s %n", w_target, w_explain);
            }
        }
    }
}
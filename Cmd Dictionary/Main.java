public class Main {
    public static void main(String[] args) {
        DictionaryCommandLine test = new DictionaryCommandLine();

        System.out.println("0. Thoát ứng dụng");
        System.out.println("1. Phiên bản Basic");
        System.out.println("2. Phiên bản Advanced");
        System.out.print("Chọn phiên bản: ");
        int chooseVersion = test.Check();

        if (chooseVersion == 0) {
            System.out.print("\n");
            System.out.println("CHƯƠNG TRÌNH KẾT THÚC!");
            System.exit(0);
        } else {
            while (chooseVersion != 0) {
                if (chooseVersion == 1) {
                    test.dictionaryBasic();
                    break;
                }

                if (chooseVersion == 2) {
                    test.dictionaryAdvanced();
                    break;
                }

                System.out.print("\n");
                System.out.print("BẠN NHẬP SAI!!!\nXIN MỜI NHẬP LẠI: ");
                chooseVersion = test.Check();

            }
            System.out.print("\n");
            System.out.println("CHƯƠNG TRÌNH KẾT THÚC!");
            System.exit(0);
        }
    }
}